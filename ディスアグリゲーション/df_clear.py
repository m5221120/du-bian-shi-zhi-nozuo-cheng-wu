###データフレームをきれいに読み込むための関数

import pandas as pd

def dataframeclear():
    #全角文字幅考慮
    pd.set_option('display.unicode.east_asian_width', True)
    #改行幅の設定
    pd.set_option('display.expand_frame_repr', False)
    #最大表示列数
    pd.set_option('display.max_columns', 100)
    #最大表示行数
    #pd.set_option('display.max_rows', 6000)