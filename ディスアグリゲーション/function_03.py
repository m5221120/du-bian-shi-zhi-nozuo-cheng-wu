
import pandas as pd
import os
import math
import datetime
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
import lightgbm as lgb
import shap
import matplotlib.dates as mdates

cd = os.getcwd()


from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
import statsmodels.api as sm
method_list = [sm, DecisionTreeRegressor(min_samples_leaf=2), RandomForestRegressor(random_state=1)]
method_list_name = ["線形回帰", "回帰木", "ランダムフォレスト"]


app_list = ["空調", "照明",  "動力", "電灯", "冷凍・冷熱", "その他"]
month_list = list(np.core.defchararray.add(np.array(["月_"]), [str(n) for n in range(2, 13)]))
hour_list = list(np.core.defchararray.add(np.array(["計測時間_"]), [str(n) for n in range(2, 25)]))
weekday_list = list(np.core.defchararray.add(np.array(["曜日_"]), [str(n) for n in ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri"]]))
col_list = ["cornflowerblue","orange","green"]


class CurrentFileWork(): #各事業所ファイルの操作
    def __init__(self, df):
        self.filename = df
        self.df = df

    def fileread(self): #ファイルの読み込み
        self.df = pd.read_csv(self.df, encoding="shift-jis")

    def df_addfeatures(self):
        list, self.ylist = [], []
        for j in range(len(self.df)):
            list.append(datetime.datetime.strptime(self.df.loc[j, "計測日"], '%Y/%m/%d').strftime('%a'))

        self.df["月"] = self.df['計測日'].str[5:7].astype(int)
        self.df["曜日"] = pd.DataFrame(list)
        self.df["気温と全体"] = self.df["気温"] * self.df["全体"]
        self.df = self.df.dropna(how='all', axis=1)

        for i in range(len(self.df.columns)):
            if self.df.columns[i] in app_list:
                self.ylist.append(self.df.columns[i]) #計測された用途のリスト
                self.df["pre_{}".format(self.df.columns[i])] = self.df[self.df.columns[i]].shift(1) #一時間前値の作成
                self.df.loc[0, "pre_{}".format(self.df.columns[i])] = self.df.loc[1, "pre_{}".format(self.df.columns[i])]
        print("データのあるylist{}".format(self.ylist))

    def df_daily(self):
        self.daily_app = self.df.groupby('計測日').agg({ylist: sum for ylist in self.ylist}) #用途別の日量換算
        self.daily_overall = self.df.groupby('計測日').agg({'全体': sum}) #全体の日量換算
        self.daily_app = self.daily_app.reset_index()
        self.start_date = str(self.daily_app["計測日"].values[0])
        print("計測開始日:{}".format(self.start_date))


    def linegraph_view(self):
        x = pd.date_range(self.start_date, periods=365, freq='d')
        fig, ax = plt.subplots(figsize=(16, 9))
        self.daily_app.set_index(x).plot(ax=ax, legend=False)

        ax.xaxis.set_major_formatter(mdates.DateFormatter("%m/%d"))
        ax.xaxis.set_major_locator(mdates.MonthLocator())
        for tick in ax.get_xticklabels():
            tick.set_rotation(90)
        plt.title("Annual daily volume transition (id = %d)" % (int(str(self.filename)[5:9])), fontsize=15)
        plt.xlabel("Measured Date", fontsize=10)
        plt.ylabel("Application Demand (kW)", fontsize=10)
        plt.legend(prop={'size':14,})
        plt.gca().yaxis.grid(True)
        plt.tick_params(labelsize=20)
        plt.minorticks_off() #補助目盛off

        os.chdir(cd + "/作図/")
        #plt.savefig("{}_日量推移.jpg".format(int(str(filename)[5:9])))
        plt.show()


    def dummy_change(self, i):
        if i == 2: ##sklearnのとき必要なダミー変換
            self.df["連続計測時間"] = self.df["計測時間"]
            self.df = pd.get_dummies(data=self.df, columns=["月", "曜日", "計測時間"])


    def model_value(self, i):
        self.y_app = self.ylist[i]
        self.xlist = ["計測日", "連続計測時間", "気温", "全体", "pre_{}".format(self.y_app), "気温と全体"]
        # xlist.extend(month_list) #月変数の追加
        self.xlist.extend(hour_list)  # 時間の追加
        self.xlist.extend(weekday_list)  # 曜日の追加
        # print("説明変数xlist:{}".format(xlist))
        # print("目的変数:{}".format(ylist))

        # テストデータを各月5の倍数の日
        self.df_train = self.df[self.df["計測日"].str[8:].astype(int) % 5 != 0]
        self.df_test = self.df[self.df["計測日"].str[8:].astype(int) % 5 == 0]
        self.date_X_train, self.y_train = np.array(self.df_train[self.xlist]), np.array(self.df_train[self.y_app])
        self.date_X_test, self.y_test = np.array(self.df_test[self.xlist]), np.array(self.df_test[self.y_app])

        self.shap_num = 0
        print("計測日:{}, 計測時間:{}, 実測値:{}".format(self.date_X_test[self.shap_num,0], self.date_X_test[self.shap_num,1], self.y_test[self.shap_num]))

        self.X_train, self.X_test = self.date_X_train[:, 2:], self.date_X_test[:, 2:]




    def lgb_model(self):
        print("================lightgbm=====================")
        lgb_train = lgb.Dataset(self.X_train, self.y_train)
        lgb_eval = lgb.Dataset(self.X_test, self.y_test, reference=lgb_train)
        lgbm_params = {
            'objective': 'regression',
            'metric': 'rmse',
        }
        self.model = lgb.train(lgbm_params, lgb_train, valid_sets=lgb_eval)
        self.ypred = self.model.predict(self.X_test, num_iteration=self.model.best_iteration)

        explainer = shap.TreeExplainer(self.model)
        shap_values = explainer.shap_values(self.X_test)
        print("base value={}".format(explainer.expected_value))
        #shap.force_plot(explainer.expected_value, shap_values[0, :], self.X_test[0, :], feature_names=self.xlist[2:], matplotlib=True, figsize=(10,5))
        #shap.summary_plot(shap_values, self.X_test, feature_names=self.xlist[2:])
        #lgb.plot_importance(bst, max_num_features=10)
        #shap.dependence_plot('RM', shap_values, self.X_test)


    def sklearn_model(self):
        print("== == == == == == == == randomforest == == == == == == == == == == = ")
        self.model = method_list[2].fit(self.X_train[:, 2:], self.y_train) #学習 #1行目は日付
        self.ypred = self.model.predict(self.X_test[:, 2:]) #予測

    def pre_error(self, i):
        self.mse = mean_squared_error(self.y_test, self.ypred)
        self.result_df = pd.DataFrame(np.c_[self.date_X_test[:, :2], self.y_test, self.ypred])
        self.pre_err = list((self.result_df.loc[:, 3] - self.result_df.loc[:, 2]) / self.result_df.loc[:, 2]) #予測誤差の計算

        if i == 2:
            self.pre_err_all = np.array(self.pre_err)
        else:
            self.pre_err_all = np.c_[self.pre_err_all, np.array(self.pre_err)]

        self.result_df.columns = ['計測日', '計測時間', '実測値', "予測値"]
        self.result_df = self.result_df.sort_values(['計測日', "計測時間"])
        self.result_df = self.result_df.reset_index(drop=True)

        """
        b_list = [i for i, x in enumerate(pre_err0) if x > 4] #予測誤差4以上抜きだし
        for i in b_list:
            test_list = list(result0_df.iloc[i, 2:4])
            test_list.append(pre_err0[i])
            print(test_list)
            
        """

    def scatter_transit_view(self):
        fig1 = plt.figure(figsize=(15, 8))
        fig1.subplots_adjust(wspace=0.4, hspace=0.4)
        self.graph_list1 = []
        for j in range(6): self.graph_list1.append("ax{}".format(j))  # graph_list1にax0,as1,...ax5を追加
        for j in range(6): self.graph_list1[j] = fig1.add_subplot(2, 3, j + 1)

        ###散布図
        self.graph_list1[0].scatter(self.y_test, self.ypred, marker=".", color="royalblue")
        self.min_a = math.ceil((min(np.amin(self.y_test), np.amin(self.ypred)) - 10) / 10)*10
        self.max_a = int((max(np.amax(self.y_test), np.amax(self.ypred)) + 10) / 10) * 10
        self.sub = self.max_a - self.min_a
        self.x = np.arange(self.min_a, self.max_a)
        self.graph_list1[0].axis("square")
        self.graph_list1[0].plot(self.x, self.x, color = "red") #y=x
        self.graph_list1[0].set_xlim(self.min_a, self.max_a), self.graph_list1[0].set_ylim(self.min_a, self.max_a) #軸の値の範囲指定
        self.graph_list1[0].set_xticks([self.min_a,self.min_a+self.sub/4,self.min_a+self.sub/2,self.min_a+3*self.sub/4,self.max_a])
        self.graph_list1[0].set_yticks([self.min_a,self.min_a+self.sub/4,self.min_a+self.sub/2,self.min_a+3*self.sub/4,self.max_a])
        self.graph_list1[0].set_ylabel("Predicted Values of Demand (kw)"), self.graph_list1[0].set_xlabel("Real Values of Demand (kw)")
        self.graph_list1[0].grid()
        self.graph_list1[0].set_title("MSE={}".format(round(self.mse, 3)), loc="right", size=9) #MSEの表示


        ####特定の日付対象
        md_str = ["/04/15", "/08/15", "/10/15", "/01/15"]
        season = ["Spring", "Summer", "", "Autumn", "Winter"]
        i=0
        for date in md_str:
            i = i + 1
            self.result_df_temp = self.result_df[self.result_df["計測日"].str.contains(date)].reset_index(drop=True)
            self.graph_list1[i].plot(self.result_df_temp["実測値"], label="Real Value", marker=".")
            self.graph_list1[i].plot(self.result_df_temp["予測値"], label="Predicted Value", marker="+", linestyle="dashdot", linewidth=1.5)
            self.graph_list1[i].set_title("{}\nTarget Date:{}".format(season[i-1], self.result_df_temp.iloc[0,0]), fontsize=10)
            self.graph_list1[i].set_xlabel("Time (hour)"), self.graph_list1[i].set_ylabel("Demand of refrigeration / cooling (kW)")
            self.graph_list1[i].set_xticks(np.arange(0, 24, 2)+1) #目盛りの幅を設定
            self.graph_list1[i].set_xticklabels(list(np.arange(0, 24, 2)+2)) #目盛りの値を設定
            self.graph_list1[i].legend(loc="upper left")
            if i == 2: i = i + 1
        fig1.suptitle("Forecasting hourly demand(id={})\nTarget Variable:{}\n".format(int(str(self.filename)[5:9]), self.y_app))

        os.chdir(cd + "/作図/")
        #plt.savefig("{}_{}.jpg".format(int(str(filename)[5:9]), ylist))
        plt.show()




    def boxplot_view(self):

        # 箱ひげ図
        plt.figure(figsize=(6, 8))
        #plt.subplot(311)
        bp2 = plt.boxplot(self.pre_err_all, patch_artist=False, widths=0.3, sym="x")
        plt.grid(color="gray", linestyle="dashed", axis="y")
        plt.ylabel("予測誤差 ( = (予測値 - 実測値) / 実測値 )")
        plt.xticks(range(1,len(self.ylist)+1), self.ylist, rotation=90)
        plt.title("id={}".format(int(str(self.filename)[5:9])), loc="left", size=8)


        for box in bp2['boxes']:
            box.set(color="royalblue", linewidth=1.5)
        for box in bp2['whiskers']:
            plt.setp(box, ls="solid", color="royalblue", linewidth=1.5)
        for box in bp2['caps']:
            plt.setp(box, color="royalblue", linewidth=1.5)
        for box in bp2['medians']:
            plt.setp(box, color="black", linewidth=1.5)

        os.chdir(cd + "/作図/")
        #plt.savefig("{}_box.jpg".format(int(str(filename)[5:9])))

