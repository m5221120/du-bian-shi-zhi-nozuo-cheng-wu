import os
import pandas as pd
import numpy as np
from df_clear import dataframeclear
import lightgbm as lgb
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import matplotlib.pyplot as plt


class Super_Tpr_List_Revise():
    def __init__(self, df):
        self.df = pd.read_csv("super_tpr.csv", index_col=0)
        self.df_colname = list(self.df.columns)
        self.ctg_list = ["id", "曜日"]
        self.id_list = self.df["id"].unique() #idリスト
        self.tpr_list = ["空調", "冷凍・冷熱"]
        self.year_hour = 8760
        self.test_buildnum = 43
        self.test_num = self.year_hour * self.test_buildnum
        self.weekday_dict = {"Sunday": 0, "Monday": 1, "Tuesday": 2, "Wednesday": 3,
                             "Thursday": 4, "Friday": 5, "Saturday": 6}
        self.lgbm_params = {'objective': 'regression', 'metric': 'rmse',
                            "max_depth": 3,
                            "min_data_in_leaf": 5, #min_data_in_leafによりWarning発生
                            }
        # self.df_eval = pd.DataFrame(index=["RMSE", "MAE"])
        self.df_eval = {}

    def type_change(self):
        self.df["曜日"] = self.df["曜日"].map(self.weekday_dict)
        for i, ctg in enumerate(self.ctg_list):
            self.df[ctg] = self.df[ctg].astype("category")

    def app_tpr_loop(self):
        for i, app in enumerate(self.tpr_list): #用途ループ
            self.df_eval[app] = pd.DataFrame(index=["RMSE", "MAE"])
            print("i = {}, 目的変数:{}".format(i, app))
            self.df_y = self.df[app]
            self.df_X = self.df.drop(self.tpr_list, axis=1)
            self.drop_list = self.tpr_list.copy()
            self.drop_list.append("pre_{}".format(self.tpr_list[(i + 1) % 2]))
            self.df_X = self.df.drop(self.drop_list, axis=1)

            for j in range(5): #CVループ
                start, end = j*self.test_num, (j+1)*self.test_num
                self.X_test, self.y_test = self.df_X.iloc[start:end, :], self.df_y.iloc[start:end]
                self.X_train = pd.concat([self.df_X.iloc[:start, :], self.df_X.iloc[end:, :]])
                self.y_train = pd.concat([self.df_y.iloc[:start], self.df_y.iloc[end:]])

                lgb_train = lgb.Dataset(self.X_train, self.y_train)
                lgb_eval = lgb.Dataset(self.X_test, self.y_test, reference=lgb_train)
                self.model = lgb.train(self.lgbm_params, lgb_train, valid_sets=lgb_eval)
                self.y_pred = self.model.predict(self.X_test, num_iteration=self.model.best_iteration)

                #self.pred_error = (self.y_test.values - self.y_pred) / self.y_pred

                rmse = np.sqrt(mean_squared_error(self.y_pred, self.y_test))
                mae = mean_absolute_error(self.y_pred, self.y_test)
                self.df_eval[app]["{}回目".format(j+1)] = [rmse, mae]
                print(self.df_eval[app])

        for app in self.df_eval.keys():
            self.df_eval[app]["平均"] = self.df_eval[app].mean(axis="columns")
            print(app)
            print(self.df_eval[app])
            self.df_eval[app].to_csv("{}_eval.csv".format(app))



            """
            plt.plot(range(self.year_hour), self.pred_error[:self.year_hour])
            plt.figure(figsize=(6, 8))
            bp2 = plt.boxplot(self.pred_error, patch_artist=False, widths=0.3, sym="x")
            plt.show()
            """

            """
            #lgb.plot_importance(self.model, importance_type="gain", figsize=(12, 6))
            plt.show()

            #explainer = shap.TreeExplainer(self.model)
            #shap_values = explainer.shap_values(self.X_test)
            #print("base value={}".format(explainer.expected_value))
            #shap.force_plot(explainer.expected_value, shap_values[0, :], self.X_test[0, :], feature_names=self.xlist[2:], matplotlib=True, figsize=(10,5))
            #shap.summary_plot(shap_values, self.X_test, feature_names=self.xlist[2:])
            #lgb.plot_importance(bst, max_num_features=10)
            #shap.dependence_plot('RM', shap_values, self.X_test)

            """
if __name__ == "__main__":
    dataframeclear()
    os.chdir("../")
    stlr = Super_Tpr_List_Revise("super_tpr.csv")
    stlr.type_change()
    stlr.app_tpr_loop()




