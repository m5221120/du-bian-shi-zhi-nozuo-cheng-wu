import os
import pandas as pd
from df_clear import dataframeclear

dataframeclear()
os.chdir("../")
df = pd.read_csv("sm_application.csv")
df["six_digits"] = df["six_digits"].map(lambda s:str(s).rjust(6, "0")).astype(str)
application_list = ["空調", "照明", "動力", "電灯", "冷凍・冷熱", "その他"]

print(df.loc[:, application_list].sum())

counted = df["six_digits"].value_counts()
count_list = []

for i in counted.index:
    count_list.append(list(i) + [counted[i]])
df_sort = pd.DataFrame(count_list, columns=application_list+["カウント"])
print(df_sort)
