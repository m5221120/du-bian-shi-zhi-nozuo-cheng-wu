import pandas as pd
import datetime
import numpy as np
import os
from df_clear import dataframeclear


application_list = ["空調", "照明", "動力", "電灯", "冷凍・冷熱", "その他"]
app_list_tpr = ["空調", "冷凍・冷熱"]  # 気温感応用途
app_list_other = ["動力", "照明", "電灯", "その他"]
df_values_list = ["ファイル名", "年", "月", "日", "曜日", "計測時間", "全体", "気温", "pre_空調", "pre_冷凍・冷熱", "空調", "冷凍・冷熱"]


class Aggregate_SuperMarket():
    def __init__(self, df):
        self.df = pd.read_csv(df)
        self.df  = self.df[self.df["業種"]=="スーパー"]
        self.df["filename"] = "B11" + self.df["ファイル名"]

    def application_bi(self):  # 各用途の集計
        for i, filename in enumerate(list(self.df.loc[:, "filename"])):
            self.app_dict = {"ファイル名": filename, "空調": 0, "照明": 0, "動力": 0, "電灯": 0, "冷凍・冷熱": 0, "その他": 0}
            self.df_temp = pd.read_csv(filename, encoding="shift-jis")
            self.df_temp = self.df_temp.dropna(how='any', axis=1)

            for j, app in enumerate(list(self.df_temp.columns[3:-1])):
                if app in self.app_dict.keys():
                    self.app_dict[app] = 1

            if i == 0:
                self.list_biapp = []
                self.list_biapp.append(list(self.app_dict.values()))
            else: self.list_biapp.append(list(self.app_dict.values()))
    

    def dataframe_make(self): #csvファイルで保存
        self.df_super_app = pd.DataFrame(self.list_biapp)
        self.df_super_app.columns = self.app_dict.keys()

        for i, app in enumerate(self.df_super_app.columns[1:]):
            if i == 0:
                self.df_super_app["six_digits"] = self.df_super_app[app].astype(str)
            else: self.df_super_app["six_digits"] = self.df_super_app["six_digits"] + self.df_super_app[app].astype(str)

        self.df_super_app["six_digits"] = self.df_super_app["six_digits"].astype("category")
        self.df_super_app = self.df_super_app.set_index("ファイル名", drop=True)

        print(self.df_super_app)
        print(self.df_super_app["six_digits"].value_counts())
        self.df_super_app.to_csv("../sm_application.csv")

if __name__ == "__main__":
    dataframeclear()
    os.chdir("../")
    aggsup = Aggregate_SuperMarket("alllist_2.csv")
    os.chdir("./各事業所のBEMSと気温データ")
    aggsup.application_bi()
    print(os.getcwd())
    aggsup.dataframe_make()

