from df_clear import dataframeclear
import os
import pandas as pd
from tqdm import tqdm

class Append_Dataframe():
    def __init__(self, filename):
        self.df_super = pd.read_csv(filename)
        self.application_list = ["空調", "照明", "動力", "電灯", "冷凍・冷熱", "その他"]
        self.app_list_tpr = ["空調", "冷凍・冷熱"]  # 気温感応用途
        self.app_list_nontpr = self.application_list.copy()
        for i, del_app in enumerate(self.app_list_tpr):
            self.app_list_nontpr.remove(del_app)

    def building_filter(self):
        self.df_super = self.df_super[ (self.df_super["空調"] == 1) & (self.df_super["冷凍・冷熱"] == 1)].reset_index(drop=True)

    def building_read(self):
        for i, b_name in tqdm(enumerate(list(self.df_super["ファイル名"]))): #建物のループ
            self.df_super_each = pd.read_csv(b_name, encoding="shift-jis")
            self.df_super_each = self.df_super_each.drop(self.app_list_nontpr, axis=1)
            self.df_super_each["id"] = int(b_name[5:9])

            for j, app_tpr in enumerate(self.app_list_tpr):  # 一時間前値の作成
                self.df_super_each["pre_{}".format(app_tpr)] = self.df_super_each[app_tpr].shift(1)
                self.df_super_each.loc[0, "pre_{}".format(app_tpr)] = self.df_super_each.loc[0, app_tpr]

            self.df_super_each["曜日判定"] = pd.to_datetime(self.df_super_each["計測日"])
            self.df_super_each["曜日"] = self.df_super_each["曜日判定"].dt.day_name()
            self.df_super_each = self.df_super_each.drop("曜日判定", axis=1)

            self.df_super_each = pd.concat([self.df_super_each, self.df_super_each["計測日"].str.split("/", expand = True).astype(int)], axis=1).drop("計測日", axis=1)
            self.df_super_each.rename(columns={0: "年", 1: "月", 2:"日"}, inplace=True)

            # 建物を統合
            if i == 0:
                self.df_super_all = self.df_super_each
            else:
                self.df_super_all = pd.concat([self.df_super_all, self.df_super_each])
        #print(self.df_super_all, self.df_super_all.shape)
        self.df_super_all.to_csv("../super_tpr.csv")


if __name__ == "__main__":
    dataframeclear()  # データフレームをきれいに表示
    cd = os.getcwd()
    os.chdir(cd + "/../")
    ad = Append_Dataframe("sm_application.csv")  # インスタンスの生成
    ad.building_filter()
    os.chdir(cd + "/../各事業所のBEMSと気温データ")
    ad.building_read()
