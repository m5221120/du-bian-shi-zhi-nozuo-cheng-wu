import os
import matplotlib.pyplot as plt
from df_clear import dataframeclear
from all_list import Append_Dataframe
from function_03 import CurrentFileWork


dataframeclear() #データフレームをきれいに表示

"""
cd = os.getcwd()
os.chdir(cd + "/../")
sm = File_Read("sm_application.csv") #インスタンスの生成
sm.building_filter()
os.chdir(cd + "/../各事業所のBEMSと気温データ")
sm.building_read()
"""


for i in range(141,142): #建物ループ
    #141, 1051
    filename = al.filename(i) #i行目のファイル名取得
    al.info_print(i)
    cfw = CurrentFileWork(filename) #インスタンスの作成

    os.chdir(cd + "/../各事業所のBEMSと気温データ")
    cfw.fileread() #建物ファイルの読み込み


    cfw.df_addfeatures() #特徴量作成
    cfw.df_daily() #日量データフレームの計算
    cfw.linegraph_view() #折れ線グラフ描画

    #for i in range(len(ylist)): #用途別のループ
    for i in range(2,4):  # 用途別のループ
        cfw.dummy_change(i) #ダミー変換
        cfw.model_value(i) #モデルにいれる変数作成
        cfw.lgb_model()


        #cfw.sklearn_model() #モデル作成

        cfw.pre_error(i) #予測誤差の計算
        cfw.scatter_transit_view() #散布図と予測推移の表示
    #plt.show()

   
    #cfw.boxplot_view() #予測誤差の箱ひげ図の表示
    #plt.show()
    #plt.close("all")
