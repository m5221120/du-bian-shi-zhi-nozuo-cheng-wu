import os
from RFR_1_func import dataframeclear
from RFR_1_func import AllList
from RFR_1_func import CurrentFileWork
from RFR_1_func import boxplot

dataframeclear() #データフレームをきれいに表示

cd = os.getcwd()
os.chdir(cd + "/../")
al = AllList("alllist_2.csv") #インスタンスの生成
al.fileread() #ファイルの読み込み
dflen = al.dflen() #ファイル数の取得


#141, 1051
filename = al.filename(141) #i行目のファイル名取得
print("filename:{}".format(filename))
os.chdir(cd + "/../各事業所のBEMSと気温データ")
cfw = CurrentFileWork(filename) #インスタンスの作成
cfw.fileread() #建物ファイルの読み込み
df, ylist = cfw.df_addfeatures() #特徴量作成


daily_app, daily_overall, start_date = cfw.df_daily(ylist) #日量データフレームの計算
#cfw.stackedgraph(daily_app, daily_overall, start_date, filename) #積み上げグラフ描画

#pre_err1, pre_err2, pre_err3 = cfw.train3test1(df, filename, ylist[3])
cfw.train3test1(df, filename, ylist[3], start_date)

#oxplot(pre_err1, pre_err2, pre_err3)
