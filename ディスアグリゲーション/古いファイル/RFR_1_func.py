
import pandas as pd
import os
import datetime
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cross_validation import train_test_split
from sklearn.metrics import mean_squared_error
import matplotlib.dates as mdates

cd = os.getcwd()

#データフレームをきれいに読み込むための関数
def dataframeclear():
    #全角文字幅考慮
    pd.set_option('display.unicode.east_asian_width', True)
    #改行幅の設定
    pd.set_option('display.expand_frame_repr', False)
    #最大表示列数
    pd.set_option('display.max_columns', 100)
    #最大表示行数
    #pd.set_option('display.max_rows', 6000)


class AllList(): #各事業所の事業所属性の操作
    def __init__(self, df):
        self.df = df

    def fileread(self): #ファイルの読み込み
        self.df = pd.read_csv(self.df)

    def dflen(self):
        return len(self.df)

    def filename(self, i): #ファイル名、所在地
        print("所在地:{}".format(self.df.loc[i, "所在地"]))
        return "B11" + self.df.iat[i, 1]


from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
import statsmodels.api as sm
method_list = [sm, DecisionTreeRegressor(min_samples_leaf=2), RandomForestRegressor(random_state=1)]
method_list_name = ["線形回帰", "回帰木", "ランダムフォレスト"]


app_list = ["空調", "照明",  "動力", "電灯", "冷凍・冷熱", "その他"]
month_list = list(np.core.defchararray.add(np.array(["月_"]), [str(n) for n in range(2, 13)]))
hour_list = list(np.core.defchararray.add(np.array(["計測時間_"]), [str(n) for n in range(2, 25)]))
weekday_list = list(np.core.defchararray.add(np.array(["曜日_"]), [str(n) for n in ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri"]]))
col_list = ["cornflowerblue","orange","green"]
class CurrentFileWork(): #各事業所ファイルの操作
    def __init__(self, df):
        self.df = df

    def fileread(self): #ファイルの読み込み
        self.df = pd.read_csv(self.df, encoding="shift-jis")


    def df_addfeatures(self):
        list, ylist = [], []
        for j in range(len(self.df)):
            list.append(datetime.datetime.strptime(self.df.loc[j, "計測日"], '%Y/%m/%d').strftime('%a'))
        self.df["月"] = self.df['計測日'].str[5:7].astype(int)
        self.df["曜日"] = pd.DataFrame(list)
        self.df["気温と全体"] = self.df["気温"] * self.df["全体"]
        self.df = self.df.dropna(how='all', axis=1)
        for i in range(len(self.df.columns)):
            if self.df.columns[i] in app_list:
                ylist.append(self.df.columns[i])
                self.df["pre_{}".format(self.df.columns[i])] = self.df[self.df.columns[i]].shift(1)
                self.df.loc[0, "pre_{}".format(self.df.columns[i])] = self.df.loc[1, "pre_{}".format(self.df.columns[i])]
        self.df["連続計測時間"] = self.df["計測時間"]
        self.df["連続計測月"] = self.df["計測日"].str[5:7].astype(int)
        self.df = pd.get_dummies(data=self.df, columns=["月", "曜日", "計測時間"])
        print("データのあるylist{}".format(ylist))
        return self.df, ylist

    def df_daily(self, ylist):
        daily_app = self.df.groupby('計測日').agg({ylist: sum for ylist in ylist})
        daily_overall = self.df.groupby('計測日').agg({'全体': sum})
        daily_app = daily_app.reset_index()
        start_date = str(daily_app["計測日"].values[0])
        return daily_app, daily_overall, start_date

    def stackedgraph(self, daily_app, daily_overall, start_date, filename):
        x = pd.date_range(start_date, periods=365, freq='d')
        fig, ax = plt.subplots(figsize=(8, 8))
        daily_app.set_index(x).plot.area(ax=ax, stacked=True)
        #daily_overall.set_index(x).plot.area(ax=ax, stacked=False)

        ax.xaxis.set_major_formatter(mdates.DateFormatter("%m/%d"))
        ax.xaxis.set_major_locator(mdates.MonthLocator())
        for tick in ax.get_xticklabels():
            tick.set_rotation(90)
        plt.title("用途別電気使用量の日量推移(id = %d)" % (int(str(filename)[5:9])), fontsize=14)
        plt.xlabel("計測日", fontsize=14)
        plt.ylabel("使用電力量(kW)", fontsize=14)
        plt.legend(prop={'size':14,})
        plt.gca().yaxis.grid(True)
        plt.tick_params(labelsize=13)
        #os.chdir("../学会予稿")
        #plt.savefig("学会用図_日量推移_1.jpg")
        plt.show()

    def train3test1(self, df, filename, ylist, start_date):
        xlist = ["計測日", "連続計測時間", "気温", "全体", "pre_{}".format(ylist), "気温と全体"]
        xlist.extend(hour_list) #時間の追加
        #xlist.extend(weekday_list)  # 曜日の追加
        len_method = len(method_list_name)
        print("説明変数xlist:{}".format(xlist[2:]))
        print("目的変数:{}".format(ylist))
        print("計測開始日:{}".format(df.loc[0,"計測日"]))

        fig1 = plt.figure(figsize=(15, 8))
        fig1.subplots_adjust(wspace=0.4, hspace=0.4)
        graph_list1 = []
        for i in range(len_method*2): graph_list1.append("ax{}".format(i))
        for i in range(len_method*2): graph_list1[i] = fig1.add_subplot(2,len_method,i+1)
        j=0
        train_month = []
        train_month.append(int(start_date[5:7]))
        for i in range(1,12):
            if 12 in train_month:
                train_month.append(1 + j)
                j=j+1
            else:
                train_month.append(int(start_date[5:7]) + i)
        print(train_month)


        #df_train = df[df["連続計測月"]==8] #トレインデータのデータフレーム作成
        #print(df_train, df_train.shape)

        """
        df_train1 = df_train.query("気温 < 18") #①気温<18 トレイン
        X_train1, y_train1 = np.array(df_train1[xlist]), np.array(df_train1[ylist])
        model1 = method_list[2].fit(X_train1[:,2:], y_train1)
        print("(b)① n = {}".format(len(y_train1)))

        df_test = df[df["計測日"].str[8:].astype(int)%5 == 0]
        df_test1 = df_test.query("気温 < 18")  # ①気温<18 テスト
        X_test1, y_test1 = np.array(df_test1[xlist]), np.array(df_test1[ylist])
        ypred1 = model1.predict(X_test1[:, 2:])
        test1_res_df = pd.DataFrame(np.c_[X_test1[:, :2], y_test1, ypred1])
        pre_err1 = list((test1_res_df.loc[:,2] - test1_res_df.loc[:,3])/test1_res_df.loc[:,3])
        print("(b)①テストMSE={}".format(mean_squared_error(y_test1, ypred1)))



        X_train0, y_train0 = np.array(df_train[xlist]), np.array(df_train[ylist])  # 区分なし
        model0 = method_list[2].fit(X_train0[:, 2:], y_train0)
        X_test0, y_test0 = np.array(df_test[xlist]), np.array(df_test[ylist])
        ypred0 = model0.predict(X_test0[:, 2:])
        result0_df = pd.DataFrame(np.c_[X_test0[:, :2], y_test0, ypred0])
        pre_err0 = list((result0_df.loc[:, 2] - result0_df.loc[:, 3]) / result0_df.loc[:, 3])
        print("(a)テストMSE={}".format(mean_squared_error(y_test0, ypred0)))



        df_train2 = df_train.query("18<= 気温 <=24") # ②18<気温<24 トレイン
        X_train2, y_train2 = np.array(df_train2[xlist]), np.array(df_train2[ylist])
        model2 = method_list[2].fit(X_train2[:,2:], y_train2)

        df_test2 = df_test.query("18<= 気温 <=24")  # ②18<気温<24 テスト
        X_test2, y_test2 = np.array(df_test2[xlist]), np.array(df_test2[ylist])
        ypred2 = model2.predict(X_test2[:, 2:])
        test2_res_df = pd.DataFrame(np.c_[X_test2[:, :2], y_test2, ypred2])
        pre_err2 = list((test2_res_df.loc[:, 2] - test2_res_df.loc[:, 3]) / test2_res_df.loc[:, 3])
        print("(b)②テストMSE={}".format(mean_squared_error(y_test2, ypred2)))
        print("(b)② n = {}".format(len(y_train2)))

        df_train3 = df_train.query("24< 気温")  # ③24<気温 トレイン
        X_train3, y_train3 = np.array(df_train3[xlist]), np.array(df_train3[ylist])
        model3 = method_list[2].fit(X_train3[:, 2:], y_train3)

        df_test3 = df_test.query("24<気温")  # ③24<気温 テスト
        X_test3, y_test3 = np.array(df_test3[xlist]), np.array(df_test3[ylist])
        ypred3 = model3.predict(X_test3[:, 2:])
        test3_res_df = pd.DataFrame(np.c_[X_test3[:, :2], y_test3, ypred3])
        pre_err3 = list((test3_res_df.loc[:, 2] - test3_res_df.loc[:, 3]) / test3_res_df.loc[:, 3])
        print("(b)③テストMSE={}".format(mean_squared_error(y_test3, ypred3)))
        print("(b)③ n = {}".format(len(y_train3)))

        result_df = pd.concat([test1_res_df, test2_res_df, test3_res_df])
        result_df.columns = ['計測日', '計測時間', '実測値', "予測値"]
        result_df = result_df.sort_values(['計測日', "計測時間"])
        result_df = result_df.reset_index(drop=True)

        result0_df.columns = ['計測日', '計測時間', '実測値', "予測値"]
        result0_df = result0_df.sort_values(['計測日', "計測時間"])
        result0_df = result0_df.reset_index(drop=True)
        
        return pre_err1, pre_err2, pre_err3
        """



def boxplot(pre_err1, pre_err2, pre_err3, pre_err0):
    data = (pre_err3, pre_err2, pre_err1)
    plt.figure(figsize=(10,5))
    plt.subplot(212)
    bp = plt.boxplot(data, patch_artist=False, widths=0.3, sym="x", vert=False)

    i=0
    for box in bp['boxes']:
        box.set(color=col_list[i], linewidth=1.5)
        i = i + 1
    i=0
    for box in bp['whiskers']:
        plt.setp(box, ls="solid", color=col_list[int(i/2)], linewidth=1.5)
        i = i + 1
    i=0
    for box in bp['caps']:
        plt.setp(box, color=col_list[int(i/2)], linewidth=1.5)
        i=i+1
    for box in bp['medians']:
        plt.setp(box, color="black", linewidth=1.5)

    plt.show()

    # 箱ひげ図
    plt.figure(figsize=(10, 5))
    plt.subplot(312)
    bp2 = plt.boxplot(pre_err0, patch_artist=False, widths=0.3, sym="x", vert=False)
    plt.grid(color="gray", linestyle="dashed", axis="x")
    plt.xlabel("予測誤差 ( = (予測値 - 実測値) / 実測値 )")
    plt.yticks([1],['(a)(n={})'.format(len(pre_err0))])
    plt.xticks(np.arange(-0.3,0.5,0.1))
    for box in bp2['boxes']:
        box.set(color="royalblue", linewidth=1.5)
    for box in bp2['whiskers']:
        plt.setp(box, ls="solid", color="royalblue", linewidth=1.5)
    for box in bp2['caps']:
        plt.setp(box, color="royalblue", linewidth=1.5)
    for box in bp2['medians']:
        plt.setp(box, color="black", linewidth=1.5)

    #os.chdir("../学会予稿")
    #plt.savefig("学会用図_箱ひげ図_2.jpg")

    plt.show()


