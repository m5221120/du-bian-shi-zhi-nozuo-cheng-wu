import os
import numpy as np
import matplotlib.pyplot as plt

from function_03 import dataframeclear
from function_03 import AllList
from RF判別あり_1_func import CurrentFileWork
from function_03 import boxplot

dataframeclear() #データフレームをきれいに表示

cd = os.getcwd()
os.chdir(cd + "/../")
al = AllList("alllist_2.csv") #インスタンスの生成
al.fileread() #ファイルの読み込み
dflen = al.dflen() #ファイル数の取得


#141, 1051
filename = al.filename(544) #i行目のファイル名取得
print("filename:{}".format(filename))
cfw = CurrentFileWork(filename) #インスタンスの作成
os.chdir(cd + "/../各事業所のBEMSと気温データ")
cfw.fileread() #建物ファイルの読み込み

df, ylist = cfw.df_addfeatures() #特徴量作成


daily_app, daily_overall, start_date = cfw.df_daily(ylist) #日量データフレームの計算
cfw.linegraph(daily_app, daily_overall, start_date, filename) #折れ線グラフ描画



"""ここから稼働日非稼働日分類"""
import pandas as pd

os.chdir(cd + "/../ディスアグリゲーション")
print(os.getcwd())
wd_df = pd.read_excel("B11000609.xlsx")
wd_df = wd_df[["計測日", "cluster_id"]]
wd_df = wd_df[wd_df["cluster_id"]==0] #今回の場合1が稼働日
wd_df = wd_df["計測日"]
print(type(wd_df.values))
wd_np = "2012/" + wd_df.values


print("非稼働日list:{}\n非稼働日数:{}".format(wd_np, len(wd_np)))

dis_df, dis_df_day = cfw.nwday_del(wd_np, ylist) #非稼働日を削除したデータフレーム #日量df
print(dis_df)
import matplotlib.pyplot as plt
dis_df_day.plot(y=ylist)
#plt.show()
"""ここまで"""


for i in range(len(ylist)): #用途別のループ
#for i in range(1):  # 用途別のループ
    pre_err0 = cfw.learntest1(dis_df, filename, ylist[i])
    if i == 0:
        pre_err0_all = np.array(pre_err0)
    else:
        pre_err0_all = np.c_[pre_err0_all, np.array(pre_err0)]


boxplot(pre_err0_all, ylist, filename)
