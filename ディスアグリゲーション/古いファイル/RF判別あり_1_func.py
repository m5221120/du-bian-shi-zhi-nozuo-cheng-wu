
import pandas as pd
import os
import math
import datetime
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cross_validation import train_test_split
from sklearn.metrics import mean_squared_error
import matplotlib.dates as mdates

cd = os.getcwd()

from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
import statsmodels.api as sm
method_list = [sm, DecisionTreeRegressor(min_samples_leaf=2), RandomForestRegressor(random_state=1)]
#method_list_name = ["線形回帰", "回帰木", "ランダムフォレスト"]


app_list = ["空調", "照明",  "動力", "電灯", "冷凍・冷熱", "その他"]
month_list = list(np.core.defchararray.add(np.array(["月_"]), [str(n) for n in range(2, 13)]))
hour_list = list(np.core.defchararray.add(np.array(["計測時間_"]), [str(n) for n in range(2, 25)]))
weekday_list = list(np.core.defchararray.add(np.array(["曜日_"]), [str(n) for n in ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri"]]))
#col_list = ["cornflowerblue","orange","green"]
class CurrentFileWork(): #各事業所ファイルの操作
    def __init__(self, df):
        self.df = df

    def fileread(self): #ファイルの読み込み
        self.df = pd.read_csv(self.df, encoding="shift-jis")


    def df_addfeatures(self):
        list, ylist = [], []
        for j in range(len(self.df)):
            list.append(datetime.datetime.strptime(self.df.loc[j, "計測日"], '%Y/%m/%d').strftime('%a'))
        self.df["月"] = self.df['計測日'].str[5:7].astype(int)
        self.df["曜日"] = pd.DataFrame(list)
        self.df["気温と全体"] = self.df["気温"] * self.df["全体"]
        self.df = self.df.dropna(how='all', axis=1)
        for i in range(len(self.df.columns)):
            if self.df.columns[i] in app_list:
                ylist.append(self.df.columns[i])
                self.df["pre_{}".format(self.df.columns[i])] = self.df[self.df.columns[i]].shift(1)
                self.df.loc[0, "pre_{}".format(self.df.columns[i])] = self.df.loc[1, "pre_{}".format(self.df.columns[i])]
        self.df["連続計測時間"] = self.df["計測時間"]
        self.df = pd.get_dummies(data=self.df, columns=["月", "曜日", "計測時間"]) #ダミー変換
        print("データのあるylist{}".format(ylist))
        return self.df, ylist

    def df_daily(self, ylist):
        daily_app = self.df.groupby('計測日').agg({ylist: sum for ylist in ylist})
        daily_overall = self.df.groupby('計測日').agg({'全体': sum})
        daily_app = daily_app.reset_index()
        start_date = str(daily_app["計測日"].values[0])
        print("計測開始日:{}".format(start_date))
        return daily_app, daily_overall, start_date

    def linegraph(self, daily_app, daily_overall, start_date, filename):
        x = pd.date_range(start_date, periods=365, freq='d')
        fig, ax = plt.subplots(figsize=(16, 8))
        daily_app.set_index(x).plot(ax=ax)

        ax.xaxis.set_major_formatter(mdates.DateFormatter("%m/%d"))
        ax.xaxis.set_major_locator(mdates.MonthLocator())
        for tick in ax.get_xticklabels():
            tick.set_rotation(90)
        plt.title("用途別電気使用量の日量推移(id = %d)" % (int(str(filename)[5:9])), fontsize=14)
        plt.xlabel("計測日", fontsize=14)
        plt.ylabel("使用電力量(kW)", fontsize=14)
        plt.legend(prop={'size':14,})
        plt.gca().yaxis.grid(True)
        plt.tick_params(labelsize=13)
        plt.minorticks_off() #補助目盛off

        os.chdir(cd + "/作図/")
        plt.savefig("{}_日量推移.jpg".format(int(str(filename)[5:9])))
        # plt.show()

    def nwday_del(self, nw_np, ylist):
        self.df = self.df.set_index("計測日")
        self.df = self.df.drop(list(nw_np)).reset_index()
        dis_df_day = self.df.groupby('計測日').agg({ylist: sum for ylist in ylist}) #日量集計
        return self.df, dis_df_day

    def learntest1(self, df, filename, ylist):
        xlist = ["計測日", "連続計測時間", "気温", "全体", "pre_{}".format(ylist), "気温と全体"]
        # xlist.extend(month_list) #月変数の追加
        xlist.extend(hour_list)  # 時間の追加
        xlist.extend(weekday_list)  # 曜日の追加
        #print("説明変数xlist:{}".format(xlist))
        #print("目的変数:{}".format(ylist))

        fig1 = plt.figure(figsize=(15, 8))
        fig1.subplots_adjust(wspace=0.4, hspace=0.4)
        graph_list1 = []
        for i in range(6): graph_list1.append("ax{}".format(i)) #graph_list1にax0,as1,...ax5を追加
        for i in range(6): graph_list1[i] = fig1.add_subplot(2,3,i+1)

        #テストデータを各月5の倍数の日
        df_train = df[df["計測日"].str[8:].astype(int) % 5 != 0]
        df_test = df[df["計測日"].str[8:].astype(int)%5 == 0]


        X_train0, y_train0 = np.array(df_train[xlist]), np.array(df_train[ylist])
        model0 = method_list[2].fit(X_train0[:, 2:], y_train0) #学習 #1行目は日付
        X_test0, y_test0 = np.array(df_test[xlist]), np.array(df_test[ylist])
        ypred0 = model0.predict(X_test0[:, 2:]) #予測
        result0_df = pd.DataFrame(np.c_[X_test0[:, :2], y_test0, ypred0])
        pre_err0 = list((result0_df.loc[:, 2] - result0_df.loc[:, 3]) / result0_df.loc[:, 3]) #予測誤差の計算
        #print("({})MSE={}".format(ylist, mean_squared_error(y_test0, ypred0)))


        result0_df.columns = ['計測日', '計測時間', '実測値', "予測値"]
        result0_df = result0_df.sort_values(['計測日', "計測時間"])
        result0_df = result0_df.reset_index(drop=True)

        graph_list1[0].scatter(y_test0, ypred0, marker=".", color="royalblue")

        min_a = math.ceil((min(np.amin(y_test0), np.amin(ypred0)) - 10) / 10)*10
        max_a = int((max(np.amax(y_test0), np.amax(ypred0)) + 10) / 10) * 10
        sub = max_a - min_a


        x = np.arange(min_a, max_a)
        graph_list1[0].axis("square")
        graph_list1[0].plot(x, x, color = "red") #y=x
        graph_list1[0].set_xlim(min_a, max_a), graph_list1[0].set_ylim(min_a, max_a) #軸の値の範囲指定

        graph_list1[0].set_xticks([min_a,min_a+sub/4,min_a+sub/2,min_a+3*sub/4,max_a])
        graph_list1[0].set_yticks([min_a,min_a+sub/4,min_a+sub/2,min_a+3*sub/4,max_a])
        graph_list1[0].set_ylabel("電気使用量(kW)の予測値"), graph_list1[0].set_xlabel("電気使用量(kW)の実測値")
        graph_list1[0].grid()
        graph_list1[0].set_title("MSE={}".format(round(mean_squared_error(y_test0, ypred0), 3)), loc="right", size=9) #MSEの表示


        ####特定の日付対象
        md_str = ["/04/15", "/08/15", "/10/15", "/01/15"]
        season = ["春季", "夏季", "", "秋季", "冬季"]
        i=0
        for date in md_str:
            i = i + 1
            result0_df_temp = result0_df[result0_df["計測日"].str.contains(date)].reset_index(drop=True)
            graph_list1[i].plot(result0_df_temp["実測値"], label="実測値", marker=".")
            graph_list1[i].plot(result0_df_temp["予測値"], label="予測値", marker="+", linestyle="dashdot", linewidth=1.5)
            graph_list1[i].set_title("対象日:{}({})".format(result0_df_temp.iloc[0,0], season[i-1]), fontsize=10)
            graph_list1[i].set_xlabel("計測時間"), graph_list1[i].set_ylabel("電気使用量(kW)")
            graph_list1[i].set_xticks(np.arange(0, 24, 2)+1) #目盛りの幅を設定
            graph_list1[i].set_xticklabels(list(np.arange(0, 24, 2)+2)) #目盛りの値を設定
            if i == 2: i = i + 1
            if i == 1: graph_list1[i].legend(loc="upper left")

        fig1.suptitle("時間別需要の予測(id={})\n目的変数:{}\n".format(int(str(filename)[5:9]), ylist))

        os.chdir(cd + "/作図/")
        plt.savefig("{}_{}.jpg".format(int(str(filename)[5:9]), ylist))
        return pre_err0

