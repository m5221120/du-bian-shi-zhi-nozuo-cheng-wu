
import pandas as pd
import os
import datetime
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cross_validation import train_test_split
from sklearn.metrics import mean_squared_error
import matplotlib.dates as mdates

cd = os.getcwd()

#データフレームをきれいに読み込むための関数
def dataframeclear():
    #全角文字幅考慮
    pd.set_option('display.unicode.east_asian_width', True)
    #改行幅の設定
    pd.set_option('display.expand_frame_repr', False)
    #最大表示列数
    pd.set_option('display.max_columns', 100)
    #最大表示行数
    #pd.set_option('display.max_rows', 6000)


class AllList(): #各事業所の事業所属性の操作
    def __init__(self, df):
        self.df = df

    def fileread(self): #ファイルの読み込み
        self.df = pd.read_csv(self.df)

    def dflen(self):
        return len(self.df)

    def filename(self, i): #ファイル名
        return "B11" + self.df.iat[i, 1]


from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
import statsmodels.api as sm
method_list = [sm, DecisionTreeRegressor(min_samples_leaf=2), RandomForestRegressor()]
method_list_name = ["線形回帰", "回帰木", "ランダムフォレスト"]


app_list = ["空調", "照明",  "動力", "電灯", "冷凍・冷熱", "その他"]
month_list = list(np.core.defchararray.add(np.array(["月_"]), [str(n) for n in range(2, 13)]))
hour_list = list(np.core.defchararray.add(np.array(["計測時間_"]), [str(n) for n in range(2, 25)]))
weekday_list = list(np.core.defchararray.add(np.array(["曜日_"]), [str(n) for n in ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri"]]))

class CurrentFileWork(): #各事業所ファイルの操作
    def __init__(self, df):
        self.df = df

    def fileread(self): #ファイルの読み込み
        self.df = pd.read_csv(self.df, encoding="shift-jis")


    def df_addfeatures(self):
        list, ylist = [], []
        for j in range(len(self.df)):
            list.append(datetime.datetime.strptime(self.df.loc[j, "計測日"], '%Y/%m/%d').strftime('%a'))
        self.df["月"] = self.df['計測日'].str[5:7].astype(int)
        self.df["曜日"] = pd.DataFrame(list)
        self.df["気温と全体"] = self.df["気温"] * self.df["全体"]
        self.df = self.df.dropna(how='all', axis=1)
        for i in range(len(self.df.columns)):
            if self.df.columns[i] in app_list:
                ylist.append(self.df.columns[i])
                self.df["pre_{}".format(self.df.columns[i])] = self.df[self.df.columns[i]].shift(1)
                self.df.loc[0, "pre_{}".format(self.df.columns[i])] = self.df.loc[1, "pre_{}".format(self.df.columns[i])]
        self.df["連続計測時間"] = self.df["計測時間"]
        self.df = pd.get_dummies(data=self.df, columns=["月", "曜日", "計測時間"])
        print("データのあるylist{}".format(ylist))
        return self.df, ylist

    def df_daily(self, ylist):
        daily_app = self.df.groupby('計測日').agg({ylist: sum for ylist in ylist})
        daily_overall = self.df.groupby('計測日').agg({'全体': sum})
        daily_app = daily_app.reset_index()
        start_date = str(daily_app["計測日"].values[0])
        return daily_app, daily_overall, start_date

    def stackedgraph(self, daily_app, daily_overall, start_date, filename):
        x = pd.date_range(start_date, periods=365, freq='d')
        fig, ax = plt.subplots()
        daily_app.set_index(x).plot.area(ax=ax, stacked=True)
        daily_overall.set_index(x).plot.area(ax=ax, stacked=False)

        ax.xaxis.set_major_formatter(mdates.DateFormatter("%m/%d"))
        ax.xaxis.set_major_locator(mdates.MonthLocator())
        for tick in ax.get_xticklabels():
            tick.set_rotation(90)
        plt.title("日量推移(id = %d)" % (int(str(filename)[5:9])))
        plt.xlabel("計測日")
        plt.ylabel("使用電力量")
        plt.gca().yaxis.grid(True)
        #plt.savefig("a.pdf")
        plt.show()

    #①気温<18 ②18<=気温<=24 ③24<気温
    def temparaturethreesection(self, df, filename, ylist):
        xlist = ["計測日", "連続計測時間", "気温", "全体", "pre_{}".format(ylist), "気温と全体"]
        #xlist.extend(month_list) #月変数の追加
        xlist.extend(hour_list) #時間の追加
        #xlist.extend(weekday_list)  # 曜日の追加
        len_method = len(method_list_name)
        print("説明変数xlist:{}".format(xlist[2:]))
        print("目的変数:{}".format(ylist))
        print("計測開始日:{}".format(df.loc[0,"計測日"]))

        fig1 = plt.figure(figsize=(15, 8))
        graph_list1 = []
        for i in range(len_method*2): graph_list1.append("ax{}".format(i))
        for i in range(len_method*2): graph_list1[i] = fig1.add_subplot(2,len_method,i+1)

        df_train1 = df[df["計測日"].str[8:].astype(int) % 5 != 0]
        #df_train1 = df_train.query("気温 < 18") #①気温<18 トレイン
        X_train1, y_train1 = np.array(df_train1[xlist]), np.array(df_train1[ylist])
        model1 = method_list[2].fit(X_train1[:,2:], y_train1)

        df_test1 = df[df["計測日"].str[8:].astype(int)%5 == 0]
        #df_test1 = df_test.query("気温 < 18")  # ①気温<18 テスト
        X_test1, y_test1 = np.array(df_test1[xlist]), np.array(df_test1[ylist])
        ypred1 = model1.predict(X_test1[:, 2:])
        result_df = pd.DataFrame(np.c_[X_test1[:, :2], y_test1, ypred1])
        pre_err1 = list((result_df.loc[:,2] - result_df.loc[:,3])/result_df.loc[:,3])
        print("①テストMSE={}".format(mean_squared_error(y_test1, ypred1)))

        result_df.columns = ['計測日', '計測時間', '実測値', "予測値"]
        result_df = result_df.sort_values(['計測日', "計測時間"])
        result_df = result_df.reset_index(drop=True)

        graph_list1[1].boxplot(pre_err1)
        graph_list1[1].set_title("予測誤差の箱ひげ図\n予測誤差=(予測値-実測値)/実測値", fontsize=10)
        graph_list1[1].set_xticklabels('①(n={})'.format(len(pre_err1)), fontsize=8)


        for i in range(1):
            graph_list1[i].scatter(y_test1, ypred1, label="①気温<18", marker=".")
            graph_list1[i].set_xlim(40, 180)
            graph_list1[i].set_ylim(40, 180)
            graph_list1[i].set_title("サンプル数:{}(各月5の倍数の日)".format(len(result_df)), fontsize=10)


        ####特定の日付対象
        md_str = ["2012/08/15", "2012/10/15", "2013/01/15"]
        i=3
        for date in md_str:
            print("対象日付:{}".format(date))
            result_df_temp = result_df[result_df["計測日"] == date]
            result_df_temp = result_df_temp.reset_index(drop=True)
            print(result_df_temp)

            graph_list1[i].plot(result_df_temp["実測値"], label="実測値", marker = ".")
            graph_list1[i].plot(result_df_temp["予測値"], label="予測値", marker = "D", linestyle="dashed")
            graph_list1[i].set_title("{}".format(date), fontsize=10)
            graph_list1[i].set_xticks(np.arange(24))
            graph_list1[i].set_xticklabels(list(result_df_temp["計測時間"]), rotation=90)
            graph_list1[i].legend(loc = "upper left")
            i=i+1
        os.chdir("../作図")
        fig1.suptitle("時間別需要の予測(id={})\n説明変数:{}\n".format(int(str(filename)[5:9]), ylist))
        plt.savefig("試し_2.pdf")

        plt.show()

    """
def graph_fi(xlist, fi1, fi2, fi3, fi1_2, fi2_2, fi3_2):
    fig = plt.figure(figsize=(15, 8))
    graph_list = []
    fi_list = np.vstack([fi1, fi2, fi3, fi1_2, fi1_2, fi2_2, fi3_2])
    title_list = ["①トレイン", "②トレイン",  "③トレイン", "①テスト", "②テスト", "③テスト"]
    xlist = np.array(xlist)
    #print("xlist:{}, {}".format(xlist, type(xlist)))
    for i in range(6): graph_list.append("ax{}".format(i))
    for i in range(6): graph_list[i] = fig.add_subplot(2, 3, i + 1)
    for i in range(6):
        list = fi_list[i,:]
        indices = np.argsort(fi_list[i,:])[::-1]
        graph_list[i].bar(range(len(list)), list[indices], color='lightblue', align='center')
        graph_list[i].set_xticks(range(len(list)))
        graph_list[i].set_xticklabels(xlist[indices], rotation=90, fontsize=7)
        graph_list[i].set_title(title_list[i])

    plt.subplots_adjust(wspace=0.4, hspace=0.6)
    fig.suptitle("ランダムフォレストの特徴量重要度")
    #fig.savefig("特徴量重要度.pdf")
    plt.show()
    """


