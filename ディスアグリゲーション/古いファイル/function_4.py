
import pandas as pd
import os
import datetime
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cross_validation import train_test_split
from sklearn.metrics import mean_squared_error
import matplotlib.dates as mdates

cd = os.getcwd()

#データフレームをきれいに読み込むための関数
def dataframeclear():
    #全角文字幅考慮
    pd.set_option('display.unicode.east_asian_width', True)
    #改行幅の設定
    pd.set_option('display.expand_frame_repr', False)
    #最大表示列数
    pd.set_option('display.max_columns', 100)
    #最大表示行数
    #pd.set_option('display.max_rows', 6000)


class AllList(): #各事業所の事業所属性の操作
    def __init__(self, df):
        self.df = df

    def fileread(self): #ファイルの読み込み
        self.df = pd.read_csv(self.df)

    def dflen(self):
        return len(self.df)

    def filename(self, i): #ファイル名
        return "B11" + self.df.iat[i, 1]


from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
import statsmodels.api as sm
method_list = [sm, DecisionTreeRegressor(min_samples_leaf=2), RandomForestRegressor()]
method_list_name = ["線形回帰", "回帰木", "ランダムフォレスト"]


app_list = ["空調", "照明",  "動力", "電灯", "冷凍・冷熱", "その他"]
month_list = list(np.core.defchararray.add(np.array(["月_"]), [str(n) for n in range(2, 13)]))
hour_list = list(np.core.defchararray.add(np.array(["計測時間_"]), [str(n) for n in range(2, 25)]))
weekday_list = list(np.core.defchararray.add(np.array(["曜日_"]), [str(n) for n in ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri"]]))

class CurrentFileWork(): #各事業所ファイルの操作
    def __init__(self, df):
        self.df = df

    def fileread(self): #ファイルの読み込み
        self.df = pd.read_csv(self.df, encoding="shift-jis")

    def df_addfeatures(self):
        list, ylist = [], []
        for j in range(len(self.df)):
            list.append(datetime.datetime.strptime(self.df.loc[j, "計測日"], '%Y/%m/%d').strftime('%a'))
        self.df["月"] = self.df['計測日'].str[5:7].astype(int)
        self.df["曜日"] = pd.DataFrame(list)
        self.df["気温と全体"] = self.df["気温"] * self.df["全体"]
        self.df = self.df.dropna(how='all', axis=1)
        for i in range(len(self.df.columns)):
            if self.df.columns[i] in app_list:
                ylist.append(self.df.columns[i])
                self.df["pre_{}".format(self.df.columns[i])] = self.df[self.df.columns[i]].shift(1)
                self.df.loc[0, "pre_{}".format(self.df.columns[i])] = self.df.loc[1, "pre_{}".format(self.df.columns[i])]
        self.df = pd.get_dummies(data=self.df, columns=["月", "曜日", "計測時間"])
        print("データのあるylist{}".format(ylist))
        return self.df, ylist

    def df_daily(self, ylist):
        daily_app = self.df.groupby('計測日').agg({ylist: sum for ylist in ylist})
        daily_overall = self.df.groupby('計測日').agg({'全体': sum})
        daily_app = daily_app.reset_index()
        start_date = str(daily_app["計測日"].values[0])
        return daily_app, daily_overall, start_date

    def stackedgraph(self, daily_app, daily_overall, start_date, filename):
        x = pd.date_range(start_date, periods=365, freq='d')
        fig, ax = plt.subplots()
        daily_app.set_index(x).plot.area(ax=ax, stacked=True)
        daily_overall.set_index(x).plot.area(ax=ax, stacked=False)

        ax.xaxis.set_major_formatter(mdates.DateFormatter("%m/%d"))
        ax.xaxis.set_major_locator(mdates.MonthLocator())
        for tick in ax.get_xticklabels():
            tick.set_rotation(90)
        plt.title("日量推移(id = %d)" % (int(str(filename)[5:9])))
        plt.xlabel("計測日")
        plt.ylabel("使用電力量")
        plt.gca().yaxis.grid(True)
        #plt.savefig("a.pdf")
        plt.show()

    #①気温<18 ②18<=気温<=24 ③24<気温
    def temparaturethreesection(self, df, filename, ylist):
        xlist = ["気温", "全体", "pre_{}".format(ylist), "気温と全体"]
        #xlist.extend(month_list) #月変数の追加
        xlist.extend(hour_list) #時間の追加
        #xlist.extend(weekday_list)  # 曜日の追加
        len_method = len(method_list_name)
        print("説明変数xlist:{}".format(xlist))
        print("目的変数:{}".format(ylist))

        fig = plt.figure(figsize=(15, 8))
        graph_list = []
        for i in range(len_method*2): graph_list.append("ax{}".format(i))
        for i in range(len_method*2): graph_list[i] = fig.add_subplot(2,len_method,i+1)

        df_temp = df.query("気温 < 18") #①気温<18
        X1, y1 = np.array(df_temp[xlist]), np.array(df_temp[ylist])
        X_train1, X_test1, y_train1, y_test1 = train_test_split(X1, y1, test_size=0.3, random_state=1)
        print(X_train1)

        df_temp2 = df.query("18<= 気温 <=24")  # ②18<=気温<=24
        X2, y2 = np.array(df_temp2[xlist]), np.array(df_temp2[ylist])
        X_train2, X_test2, y_train2, y_test2 = train_test_split(X2, y2, test_size=0.3, random_state=1)

        df_temp3 = df.query("気温 > 24")  # ③24<気温
        X3, y3 = np.array(df_temp3[xlist]), np.array(df_temp3[ylist])
        X_train3, X_test3, y_train3, y_test3 = train_test_split(X3, y3, test_size=0.3, random_state=1)

        for method_num in range(len(method_list_name)):

            print("Method:{}".format(method_list_name[method_num]))
            # ①トレイン
            if method_num == 0:
                model = method_list[method_num].OLS(y_train1, X_train1).fit()
                #print(model.summary())
            elif method_num==1:
                model = method_list[method_num].fit(X_train1, y_train1)
            else: model = method_list[method_num].fit(X_train1, y_train1)
            y_pred1_train = model.predict(X_train1)
            print("①MSE for train: {:.5f}".format(mean_squared_error(y_train1, y_pred1_train)))
            graph_list[method_num].scatter(y_train1, y_pred1_train, label="①気温<18", marker="x")
            if method_num == 2:
                fi_train1 = model.feature_importances_
                #print("FeatureImportance: {}".format(fi_train1))


            # ②トレイン
            if method_num == 0:
                model = method_list[method_num].OLS(y_train2, X_train2).fit()
                #print(model.summary())
            elif method_num == 1:
                model = method_list[method_num].fit(X_train2, y_train2)
            else:
                model = method_list[method_num].fit(X_train2, y_train2)
            y_pred2_train = model.predict(X_train2)
            print("②MSE for train: {:.5f}".format(mean_squared_error(y_train2, y_pred2_train)))
            graph_list[method_num].scatter(y_train2, y_pred2_train, label="②18<=気温<=24", marker=".")
            if method_num == 2:
                fi_train2 = model.feature_importances_
                #print("FeatureImportance: {}".format(fi_train2))

            # ③トレイン
            if method_num == 0:
                model = method_list[method_num].OLS(y_train3, X_train3).fit()
                #print(model.summary())
            elif method_num == 1:
                model = method_list[method_num].fit(X_train3, y_train3)
            else:
                model = method_list[method_num].fit(X_train3, y_train3)
            y_pred3_train = model.predict(X_train3)
            print("③MSE for train: {:.5f}".format(mean_squared_error(y_train3, y_pred3_train)))
            graph_list[method_num].scatter(y_train3, y_pred3_train, label="③24<気温", marker="+")
            if method_num == 2:
                fi_train3 = model.feature_importances_
                #print("FeatureImportance: {}".format(fi_train3))

            # ①テスト
            if method_num == 0:
                model = method_list[method_num].OLS(y_test1, X_test1).fit()
                #print(model.summary())
                fig.legend()
            elif method_num == 1:
                model = method_list[method_num].fit(X_test1, y_test1)
            else:
                model = method_list[method_num].fit(X_test1, y_test1)
            y_pred1_test = model.predict(X_test1)
            print("①MSE for test: {:.5f}".format(mean_squared_error(y_test1, y_pred1_test)))
            graph_list[method_num+len_method].scatter(y_test1, y_pred1_test, label="①気温<18", marker="x")
            if method_num == 2:
                fi_test1 = model.feature_importances_
                #print("FeatureImportance: {}".format(fi_test1))

            # ②テスト
            if method_num == 0:
                model = method_list[method_num].OLS(y_test2, X_test2).fit()
                #print(model.summary())
            elif method_num == 1:
                model = method_list[method_num].fit(X_test2, y_test2)
            else:
                model = method_list[method_num].fit(X_test2, y_test2)
            y_pred2_test = model.predict(X_test2)
            print("②MSE for test: {:.5f}".format(mean_squared_error(y_test2, y_pred2_test)))
            graph_list[method_num + len_method].scatter(y_test2, y_pred2_test, label="②18<=気温<=24", marker=".")
            if method_num == 2:
                fi_test2 = model.feature_importances_
                #print("FeatureImportance: {}".format(fi_test2))

            # ③テスト
            if method_num == 0:
                model = method_list[method_num].OLS(y_test3, X_test3).fit()
                #print(model.summary())
            elif method_num == 1:
                model = method_list[method_num].fit(X_test3, y_test3)
            else:
                model = method_list[method_num].fit(X_test3, y_test3)
            y_pred3_test = model.predict(X_test3)
            print("③MSE for test: {:.5f}".format(mean_squared_error(y_test3, y_pred3_test)))
            graph_list[method_num + len_method].scatter(y_test3, y_pred3_test, label="③24<気温", marker="+")
            if method_num == 2:
                fi_test3 = model.feature_importances_
                #print("FeatureImportance: {}".format(fi_test3))

        min, max = 0, 200
        for i in range(len(graph_list)):
            graph_list[i].set_xlim(min, max), graph_list[i].set_ylim(min, max)
            if i < len_method:
                graph_list[i].set_title(method_list_name[i])
                graph_list[i].set_xlabel('実測値(トレーニング)'), graph_list[0].set_ylabel('予測値(トレーニング)')
            else:
                graph_list[len_method].set_ylabel('予測値(テスト)'), graph_list[i].set_xlabel('実測値(テスト)')
            graph_list[i].grid()

        fig.suptitle("時間別用途需要の予測(id = {})\n目的変数:{}\n説明変数:{}"
                     .format(int(str(filename)[5:9]),ylist, xlist[0:4]))
        os.chdir(cd + "/../作図")
        #fig.tight_layout()
        fig.savefig("{}_{}.pdf".format(int(str(filename)[5:9]), ylist))
        #plt.show()
        return xlist, fi_train1, fi_train2,  fi_train3, fi_test1, fi_test2, fi_test3

def graph_fi(xlist, fi1, fi2, fi3, fi1_2, fi2_2, fi3_2):
    fig = plt.figure(figsize=(15, 8))
    graph_list = []
    fi_list = np.vstack([fi1, fi2, fi3, fi1_2, fi1_2, fi2_2, fi3_2])
    title_list = ["①トレイン", "②トレイン",  "③トレイン", "①テスト", "②テスト", "③テスト"]
    xlist = np.array(xlist)
    #print("xlist:{}, {}".format(xlist, type(xlist)))
    for i in range(6): graph_list.append("ax{}".format(i))
    for i in range(6): graph_list[i] = fig.add_subplot(2, 3, i + 1)
    for i in range(6):
        list = fi_list[i,:]
        indices = np.argsort(fi_list[i,:])[::-1]
        graph_list[i].bar(range(len(list)), list[indices], color='lightblue', align='center')
        graph_list[i].set_xticks(range(len(list)))
        graph_list[i].set_xticklabels(xlist[indices], rotation=90, fontsize=7)
        graph_list[i].set_title(title_list[i])

    plt.subplots_adjust(wspace=0.4, hspace=0.6)
    fig.suptitle("ランダムフォレストの特徴量重要度")
    #fig.savefig("特徴量重要度.pdf")
    plt.show()


