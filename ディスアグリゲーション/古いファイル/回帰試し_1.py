import os
from function_4 import dataframeclear
from function_4 import AllList
from function_4 import CurrentFileWork
from function_4 import graph_fi
dataframeclear() #データフレームをきれいに表示

cd = os.getcwd()
os.chdir(cd + "/../")
al = AllList("alllist_2.csv") #インスタンスの生成
al.fileread() #ファイルの読み込み
dflen = al.dflen() #ファイル数の取得


#141, 1051
filename = al.filename(141) #i行目のファイル名取得
print("filename:{}".format(filename))
os.chdir(cd + "/../各事業所のBEMSと気温データ")
cfw = CurrentFileWork(filename) #インスタンスの作成
cfw.fileread() #建物ファイルの読み込み
df, ylist = cfw.df_addfeatures() #特徴量作成


#daily_app, daily_overall, start_date = cfw.df_daily(ylist) #日量データフレームの計算
#cfw.stackedgraph(daily_app, daily_overall, start_date, filename) #積み上げグラフ描画

xlist, fi_train1, fi_train2, fi_train3, fi_test1, fi_test2, fi_test3 = cfw.temparaturethreesection(df, filename, ylist[3])

#graph_fi(xlist, fi_train1, fi_train2,  fi_train3, fi_test1, fi_test2, fi_test3)