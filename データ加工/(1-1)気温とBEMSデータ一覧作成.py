import pandas as pd
import os

# dataframeをきれいに表示する
def pripre():
    #全角文字幅考慮
    pd.set_option('display.unicode.east_asian_width', True)
    #改行幅の設定
    pd.set_option('display.expand_frame_repr', False)

# ファイル名読み込み用データの作成
def filename_file(fn):
    os.chdir("../")
    filename = pd.read_csv(fn)
    filename["ファイル名"] = filename["ファイル名"].str.replace("dta","csv")
    filename = filename[["ファイル名", "所在地"]]
    print("ファイル名の読み込み成功(dataframe)")
    return filename

# 気温データファイルの作成
def temparature_file(fn):
    tem_file = pd.read_csv(fn, encoding="shift-jis")
    tem_file["月.1"], tem_file["日.1"] = tem_file["月.1"].str.replace("a", ""), tem_file["日.1"].str.replace("a", "")
    tem_file["年月日"] = tem_file["年"].astype(str) + "/" + tem_file["月.1"].astype(str) + "/" + tem_file["日.1"].astype(str)
    tem_file = tem_file.drop(tem_file[["年", "月.1", "日.1", "月", "日"]], axis=1)
    tem_file = tem_file.ix[:, ['年月日', '時', '北海道', "東北", "関東", "中部", "関西", "中国", "四国", "九州・沖縄"]]
    tem_file = tem_file.fillna(method='ffill') #空値を前の値で置き換え
    #print(tem_file.isnull().sum()) #空値の確認
    print("気温データの読み込み成功(dataframe)")
    return tem_file

pripre()
filename_df, temparature_df = filename_file("match_2.csv"), temparature_file("temparature_1.csv")
print("dataframe名: filename_df, temparature_df")
filename_df.to_csv("perfectfilename_use.csv", encoding="shift-jis", index = False)
temparature_df.to_csv("temparature_use.csv", encoding="shift-jis", index = False)
print(os.getcwd(), "にtemparature_use.csvとperfectfilename_use.csvを保存しました。")