import pandas as pd
import os
import numpy as np
import math

alpha = "F"

#ファイル名と所在地の操作
class FileNameLoc():
    def __init__(self, df):
        self.df = df

    def fileread(self): #ファイルの読み込み
        os.chdir(alpha + ":\python")
        return pd.read_csv(self.df, encoding = "shift-jis")

#気温データの操作
class TemparatureData():
    def __init__(self, df, location):
        self.df, self.loc = df, location

    def fileread(self): #ファイルの読み込みをして所在地をしぼる
        os.chdir(alpha + ":\python")
        self.df = pd.read_csv(self.df, encoding="shift-jis")
        self.df = self.df.loc[:, ["年月日", "時", self.loc]].values
        return self.df

    def staend(self, np_1, str_day): #開始日と終了日の行数を取得
        start_ix = list(np.where((np_1[:, 0] == str_day) & (np_1[:, 1] == 1)))
        start_ix.append(start_ix[0] + 8760)
        sta, end = int(start_ix[0]), int(start_ix[1])
        return sta, end

    def nprevise(self, sta, end):  # 必要な気温データに変換
        return self.df[sta:end, 2:3]

#操作対象ファイルの操作
class WorkingFile():
    def __init__(self, df):
        self.df = df

    def fileread(self): #ファイルの読み込みと開始日の文字列を取得
        os.chdir(alpha + ':/●89 BEMS公開データ(2)\●89 BEMS公開データ(2)/raw')

        if os.path.isfile(self.df) == False:
            os.chdir(alpha + ':/●89 BEMS公開データ(2)\●89 BEMS公開データ(2)/raw2')

        self.df = pd.read_csv(self.df, error_bad_lines=False).values
        return self.df, self.df[0,0]


#main関数
nameloc_work = FileNameLoc("filename_use.csv") #ファイル名と所在地の読込
nameloc_np = nameloc_work.fileread().values #ファイル名と所在地のnumpy作成
err_count1 = 0
list1 = []

for i in range(len(nameloc_np)):
#i = 784でエラーファイル名B11000882.csv 最初点が4つ多い
#i = 1742でエラーファイル名B11001960.csv 最後点が1つ多い
    #if i%10 == 0:
    print("%3d / %d" %(i, len(nameloc_np)))
    print("ファイル名:%s, 所在地:%s"%(nameloc_np[i,0], nameloc_np[i,1]))

    tem_work = TemparatureData("temparature_use.csv", nameloc_np[i,1]) #所在地列の気温データの読込
    tem_np = tem_work.fileread() #気温データをnumpy配列へ変換

    wf_work = WorkingFile(nameloc_np[i,0]) #操作対象ファイルの読込
    wf_np, start_day = wf_work.fileread() #操作対象ファイルと開始日の取得

    if len(str(start_day)) == 10:
        pass

    else:
        err_count1 = err_count1 + 1
        list1.append(i)
        print("____________________________ここがエラー___________________________________")


print("エラー:%d" %(err_count1))
print("list1:", list1)


"""
出力
エラー:56
list1: [784, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 796, 797, 798, 799, 800, 801, 802, 803, 804, 806, 807, 808, 809, 811, 976, 1028, 1055, 1277, 1739, 1740, 1741, 1742, 1743, 1744, 1864, 2152, 3005, 3006, 3821, 3824, 3863, 3864, 3865, 3866, 3867, 3868, 4249, 4250, 4251, 4252, 4253, 4258, 4260, 4446]
"""