import pandas as pd
import os

cd = os.getcwd()
print("cd = ",os.getcwd())
os.chdir("../python")

#全角文字幅考慮
pd.set_option('display.unicode.east_asian_width', True)
#改行幅の設定
pd.set_option('display.expand_frame_repr', False)

#ファイルの読み込み
df_1 = pd.read_csv("kinyu_ks.csv", index_col=0, encoding="shift-jis")
df_2 = pd.read_csv("som_id_1.csv", index_col=0, encoding="shift-jis")
df_2 = df_2.iloc[:,0:2]
df = pd.concat([df_1, df_2], axis = 1)

os.chdir("../python2")
df.to_csv("kinyu_1.csv", encoding="shift-jis")