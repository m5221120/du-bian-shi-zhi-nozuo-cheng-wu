import pandas as pd
import os
import matplotlib.pyplot as plt
from sklearn import mixture
import numpy as np
from sklearn.cluster import DBSCAN
from sklearn.cluster import Birch

cd = os.getcwd()
print("cd = ",os.getcwd())

#全角文字幅考慮
pd.set_option('display.unicode.east_asian_width', True)
#改行幅の設定
pd.set_option('display.expand_frame_repr', False)

#ファイルの読み込み
df = pd.read_csv("kinyu_1.csv", encoding="shift-jis")

#sklearn読み込みのためのnumpy配列の作成
df_xy = df.loc[:,["x", "y"]]
df_np = df_xy.values


#VBGM
vbgm = mixture.BayesianGaussianMixture(n_components=3, random_state=6).fit(df_np)
vbgm_labels = pd.DataFrame(vbgm.predict(df_np))
vbgm_labels.columns = ["vbgm_label"]
#vbgm_labelの結合
df = pd.concat([df, vbgm_labels], axis=1)

#DBSCAN
#epsは半径,min_samplesはグループ作成の最低サンプル数
db = DBSCAN(eps=2.8, min_samples=5).fit(df_np)
db_labels = pd.DataFrame(db.labels_)
db_labels.columns = ["db_label"]
#db_labelの結合
df = pd.concat([df, db_labels], axis=1)

#Birch
#thereshold は半径のしきい値,
brc = Birch(branching_factor=50, n_clusters=3, threshold=3.5, compute_labels=True).fit(df_np)
brc_labels = pd.DataFrame(brc.labels_)
brc_labels.columns = ["brc_label"]
#brc_labelの結合
df = pd.concat([df, brc_labels], axis=1)

df.to_csv("kinyu_2.csv", index= False, encoding="shift-jis")
