import pandas as pd
import os
import numpy as np

#全角文字幅考慮
pd.set_option('display.unicode.east_asian_width', True)
#改行幅の設定
pd.set_option('display.expand_frame_repr', False)

os.chdir("../")
#ファイル名読み込み用ファイルの読み込み
match_2 = pd.read_csv("match_2.csv")
match_2 = match_2.drop(match_2.iloc[:, 1:3], axis = 1)
match_2["ファイル名"] = match_2["ファイル名"].str.replace("dta","csv")

#fn_1にファイル名を保持
file_name = match_2["ファイル名"].values


#月ループ配列の作成
list_month = list(range(4,13))
list_month.extend(list(range(1,4)))
list_month = [str(n) for n in list_month]
k=0
for month in list_month:
    if len(month)==1 : list_month[k] = "0" + month
    k=k+1

for file_num in range(len(match_2)):
    if file_num%10 == 0:
        print('%3d / %d' % (file_num, len(match_2)))
    # 用途ループ配列
    list_app = ["全体", "空調", "照明", "動力", "電灯", "冷凍・冷熱", "その他"]

    # numpy配列の初期化
    monthly_np = np.zeros((12, 8))

    fn_1 = file_name[file_num]
    id = int(fn_1[-8:-4])
    #各事業所のファイルがあるディレクトリへ移動
    #if file_num==0: os.chdir('../●89 BEMS公開データ(2)/●89 BEMS公開データ(2)/raw')
    if file_num == 0: os.chdir('E:/●89 BEMS公開データ(2)\●89 BEMS公開データ(2)/raw')

    if os.path.isfile(fn_1) == False :
        print("%d番目の%sは存在しません。raw2に移動します。" %(file_num, fn_1))
        os.chdir('E:/●89 BEMS公開データ(2)\●89 BEMS公開データ(2)/raw2')

    #操作対象のファイルの読み込み
    df_fn_1 = pd.read_csv(fn_1)


    i=0
    #用途別月集計ループ
    for month in list_month:
        #print("%s月" %month)
        df_fn_2 = df_fn_1[df_fn_1['計測日'].str.contains('..../' + month + '/..')]

        j = 0
        # monthの代入
        monthly_np[i, j] = month

        for app_name in list_app:
            j = j + 1
            total_month = sum(df_fn_2[app_name])
            #print("j=%d" %j, app_name, total_month)
            monthly_np[i, j] = total_month
        i = i + 1

    monthly_df = pd.DataFrame(monthly_np).fillna(0).astype('int')

    list_app.insert(0, "計測月")
    monthly_df.columns = list_app
    monthly_df["id"] = id
    monthly_df = monthly_df.set_index('id')

    if file_num==0: df = monthly_df
    else: df = pd.concat([df, monthly_df])

print(df)
os.chdir("F:\python")
df.to_csv("月次データ_1.csv", encoding="shift-jis")
