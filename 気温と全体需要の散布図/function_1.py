"""
行頭に返り値の有無(○×)
●関数
×dataframeclear(): データフレームをきれいに読み込むための関数
○std_temparature(): 標準温度の設定
○subplotvalue(fig): 4x4のグラフ名の設定、グラフの間隔
○industry_list(i): リストに対応する業種の文字列を返す
"""

"""
●クラス
①AllList(): 事業所一覧ファイルへの操作
    ×def fileread(self): #ファイルの読み込み
    ○def dflen(self): #dfの長さ
    ○office_info_1(self, i): #業種、空調の有無、id
    ○office_info_2(self, id): #所在地、延床面積、契約電力

②CurrentFileWork(): 各事業所ファイルへの操作
    ×def fileread(self): #ファイルの読み込み
    ○def usage_max(self): 全体電気使用量の最大を取得
    ○def temparaturethreesection(self, i, j, tem_std_low, tem_std_high, usage_min, usage_max, graph_loc_list): 
            区間毎のデータ取得①気温<18 ②18<=気温<=24 ③24<気温
    ×def linearreg(self, i, j, X, Y, graph_loc_list): #回帰分析printも
    ×def graphoption(self, i, fig, graph_loc_list, tem_std_low, tem_std_high, usage_max, usage_min, id, totalarea, location, conpow): #グラフ描画のoption
"""


import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model

curdir = os.getcwd()

#データフレームをきれいに読み込むための関数
def dataframeclear():
    #全角文字幅考慮
    pd.set_option('display.unicode.east_asian_width', True)
    #改行幅の設定
    pd.set_option('display.expand_frame_repr', False)
    #最大表示列数
    pd.set_option('display.max_columns', 100)
    #最大表示行数
    pd.set_option('display.max_rows', 6000)

def industry_list(i):
    list = ["金融", "スーパー"]
    if i == 0: return list[0]
    elif i == 1: return list[1]

def std_temparature():
    tem_std_low = 18
    tem_std_high = 24
    return tem_std_low, tem_std_high

def subplotvalue(fig, max_count): #グラフの設定(5x5)
    list = []
    fig.subplots_adjust(wspace=0.4, hspace=0.6)  # グラフの間隔を設定
    for i in range(1, max_count+1):
        list.append(fig.add_subplot(5, 5, i))
    return list, fig

class AllList(): #各事業所の事業所属性の操作
    def __init__(self, df):
        self.df = df

    def fileread(self): #ファイルの読み込み
        os.chdir(curdir + "/../")
        self.df = pd.read_csv(self.df)

    def dflen(self):
        return len(self.df)

    def office_info_1(self, i): #
        return self.df.loc[i, "id"], self.df.loc[i, "計測対象_空調"], self.df.loc[i, "業種"],

    def office_info_2(self, id): #ファイル名,所在地,延床面積,契約電力を取得
        df_temp = self.df[self.df["id"] == id]
        return "B11" + df_temp.iat[0, 1], df_temp.iat[0, 3], df_temp.iat[0, 4], df_temp.iat[0, 22]

class CurrentFileWork(): #各事業所ファイルの操作
    def __init__(self, df):
        self.df = df

    def fileread(self): #ファイルの読み込み
        os.chdir(curdir + "/../BEMS&気温")
        self.df = pd.read_csv(self.df, encoding="shift-jis")
        self.df = self.df.loc[:, ["気温", "全体"]]  # 必要な列のみ抽出

    def usage_max(self):
        return self.df["全体"].max()  # 全体電気使用量の最大を取得

    #①気温<18 ②18<=気温<=24 ③24<気温
    def temparaturethreesection(self, i, j, tem_std_low, tem_std_high, usage_min, usage_max, graph_loc_list):
        X = []
        Y = []
        if j == 0:
            df_temp = self.df[(self.df['気温'] < tem_std_low) & (self.df["全体"] > usage_min)]
            X, Y = df_temp.loc[:, ['気温']].values, df_temp.loc[:, ['全体']].values
            graph_loc_list[i].scatter(X, Y, marker=".")

        if j == 1:
            df_temp = self.df[((self.df['気温'] <= tem_std_high) & (self.df["全体"] > usage_min)) & ((self.df['気温'] >= tem_std_low) & (self.df["全体"] > usage_min))]
            X, Y = df_temp.loc[:, ['気温']].values, df_temp.loc[:, ['全体']].values
            graph_loc_list[i].scatter(X, Y, marker=".")

        if j == 2:
            df_temp = self.df[(self.df['気温'] > tem_std_high) & (self.df["全体"] > usage_min)]
            X, Y = df_temp.loc[:, ['気温']].values, df_temp.loc[:, ['全体']].values
            graph_loc_list[i].scatter(X, Y, marker=".")

        return X, Y

    def linearreg(self, i, j, X, Y, graph_loc_list): #回帰分析
        clf = linear_model.LinearRegression()
        clf.fit(X, Y)
        graph_loc_list[i].plot(X, clf.predict(X), color="red") #グラフに反映
        #print("回帰係数(%d-%d):%f, 切片:%f, 決定係数:%f" % (i, j, clf.coef_, clf.intercept_, clf.score(X, Y)))

    def graphoption(self, i, max_count, fig, graph_loc_list, tem_std_low, tem_std_high, usage_max, usage_min, id,
                    industry, totalarea, location, conpow):
        graph_loc_list[i].set_xlabel("気温(℃)", fontsize=6)
        graph_loc_list[i].set_ylabel("電気使用量(kw)", fontsize=6)
        graph_loc_list[i].tick_params(labelsize=4)
        plt.tight_layout()

        ax_pos = graph_loc_list[i].get_position()
        if i == max_count-2: #右下に業種
            fig.text(ax_pos.x0, ax_pos.y0 - 0.05, "業種:%s(id, 所在地, 延床面積(m2), 契約電力(kW))" %industry, fontsize=10)

        fig.text(ax_pos.x0, ax_pos.y1 + 0.01, "(%d, %s, %d, %d)" % (id, location, totalarea, conpow), fontsize=8)
        graph_loc_list[i].vlines([tem_std_low], usage_min, usage_max, "blue", linestyles='dashed')
        graph_loc_list[i].vlines([tem_std_high], usage_min, usage_max, "green", linestyles='dashed')


