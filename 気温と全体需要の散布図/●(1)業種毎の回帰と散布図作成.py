import os
import matplotlib.pyplot as plt

from function_1 import dataframeclear
from function_1 import subplotvalue
from function_1 import AllList
from function_1 import std_temparature
from function_1 import CurrentFileWork
from function_1 import industry_list

curdir = os.getcwd()

dataframeclear() #データフレームをきれいに表示
al = AllList("alllist_2.csv") #クラス作成
al.fileread() #ファイルの読み込み
dflen = al.dflen() #行数取得

tem_std_low, tem_std_high = std_temparature() #標準気温の設定


ind_list = ["金融", "スーパー"]
max_count = 25 #グラフの表示枚数
count_1 = count_2 = count_3 = count_4 =  0 #カウンタ変数
id_list = [] #業種毎にidのリストを格納


#各業種に対して条件を満たすidをlistに格納
for i in range(2): #業種毎のループ
    list_temp = [] #listの初期化
    for j in range(dflen): #事業所毎のループ
        id, ac_flag, industry = al.office_info_1(j) #対象オフィスの情報保持
        if industry == ind_list[i] and ac_flag == True: #空調計測ありのみ
            count_1 = count_1 + 1
            list_temp.append(id)
    id_list.append(list_temp)
    if count_2 == 1: count_1 = 0
    print("サンプル数:%d(%s) id_list[%d]にidを格納" % ((len(id_list[i])), ind_list[i], i))
    count_1 = count_1 + 1

#業種毎に回帰分析やグラフ描画
for id_list in id_list: #業種ループ
    graph_loc_list, fig = subplotvalue(plt.figure(figsize=(11.69, 8.27)), max_count)  # グラフの設定
    for id in id_list: #事業所毎idループ
        filename, location, area, conpow = al.office_info_2(id)
        print("count_3 = %d, id = %d" % (count_3, id))
        cfw = CurrentFileWork(filename)  # インスタンス生成
        cfw.fileread()
        usage_max, usage_min = cfw.usage_max(), 5  # 電気使用量の範囲を設定

        for i in range(3):  # 気温3分割のループ
            X, Y = cfw.temparaturethreesection(count_3, i, tem_std_low, tem_std_high,
                                                usage_min, usage_max, graph_loc_list)  # 各分割毎の気温と全体を取得
            cfw.linearreg(count_3, i, X, Y, graph_loc_list)  # 回帰分析(表示と描画)
        thisindustry = industry_list(count_4)
        cfw.graphoption(count_3, max_count, fig, graph_loc_list, tem_std_low, tem_std_high, usage_max,
                        usage_min, id, thisindustry, area, location, conpow)  # 各グラフのオプション表示
        count_3 = count_3 + 1

        if count_3 == max_count:
            count_3 = 0
            os.chdir(curdir + "/../気温と全体需要")
            #plt.savefig("気温と全体需要の回帰(%s)_%d.pdf" %(thisindustry, count_3))
            plt.show()
            break
    count_4 = count_4 + 1
    print(count_4)
    #continue

