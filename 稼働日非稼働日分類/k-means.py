import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
#import japanize_matplotlib
from pandas.plotting import scatter_matrix
from pandas import Series
from pandas import DataFrame
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans

os.chdir('C:/Users/indlab/Desktop/研究(渡邊)/各事業所のBEMSと気温データ')

df = pd.read_csv('B11000609.csv',encoding='SHIFT-JIS')

data = df.loc[:,['計測日']]
data = data['計測日'].str[5:]  #2013/02/01 -> 02/01, 2014/01/31 -> 01/31 に変換

d = []      # 日付けを格納する空の配列を用意
for i in range(365):
    for j in range(24):
        j += 1
    d.append(data.iloc[i*j]) # 空の配列(d)に日付けを格納  d = (365 x 1)
d = pd.Series(d) # dはlist型なのでconcatできないから型変換する Seriesは一次元配列、DataFrameは二次元配列

data1 = df.loc[:,['全体']] #電気使用量だけを集めたデータ
data1 = data1.values
data1 = np.reshape(data1, (365,24)) # data1は8760x1の形なので365x24の形に変換

d1 = pd.DataFrame(data1)

new_data = pd.concat([d,d1],axis=1) #計測日と全体のデータを対応するように真横に連結
new_data.columns = ['計測日', '1時', '2時', '3時', '4時', '5時', '6時', '7時', '8時', '9時', '10時', '11時', '12時', '13時', '14時', '15時', '16時', '17時', '18時', '19時', '20時', '21時', '22時', '23時', '24時']
new_data['合計'] = new_data.iloc[:, 1:].sum(axis=1)
#print(new_data)
new_data.to_excel('C:/Users/Ofu/Desktop/研究/BEMS/BEMS&気温2/B11000065.xlsx')#Excelファイルに変換して保存

df = pd.read_csv('B11000132.csv',encoding='SHIFT-JIS')
data = df.loc[:,['気温']]

d2 = []      # 気温を格納する空の配列を用意
for i in range(365):
    for j in range(24):
        j += 1
    d2.append(data.iloc[i*j]) # 空の配列(d)に日付けを格納  d = (365 x 1)
d2 = pd.Series(d) # dはlist型なのでconcatできないから型変換する Seriesは一次元配列、DataFrameは二次元配列

data2 = df.loc[:,['気温']] #気温だけを集めたデータ
data2 = data2.values
data2 = np.reshape(data2, (365,24)) # data1は8760x1の形なので365x24の形に変換

d3 = pd.DataFrame(data2)

new_data1 = pd.concat([d2,d3],axis=1) #計測日と全体のデータを対応するように真横に連結
new_data1.columns = ['計測日', '1時(気温)', '2時(気温)', '3時(気温)', '4時(気温)', '5時(気温)', '6時(気温)', '7時(気温)', '8時(気温)', '9時(気温)', '10時(気温)', '11時(気温)', '12時(気温)', '13時(気温)', '14時(気温)', '15時(気温)', '16時(気温)', '17時(気温)', '18時(気温)', '19時(気温)', '20時(気温)', '21時(気温)', '22時(気温)', '23時(気温)', '24時(気温)']
new_data1['最高気温'] = new_data1.max(axis=1)
#print(new_data1)
new_data1.to_excel('C:/Users/Ofu/Desktop/研究/BEMS/気温/B11000065.xlsx')#Excelファイルに変換して保存


new_data1_1 = new_data1.drop('計測日', axis=1)
new_data2 = pd.concat([new_data,new_data1_1],axis=1)
#new_data2 = new_data2.drop(['1時', '2時', '3時', '4時', '5時', '6時', '7時', '8時', '9時', '10時', '11時', '12時', '13時', '14時', '15時', '16時', '17時', '18時', '19時', '20時', '21時', '22時', '23時', '24時','1時(気温)', '2時(気温)', '3時(気温)', '4時(気温)', '5時(気温)', '6時(気温)', '7時(気温)', '8時(気温)', '9時(気温)', '10時(気温)', '11時(気温)', '12時(気温)', '13時(気温)', '14時(気温)', '15時(気温)', '16時(気温)', '17時(気温)', '18時(気温)', '19時(気温)', '20時(気温)', '21時(気温)', '22時(気温)', '23時(気温)', '24時(気温)'], axis=1)
#print(new_data2)
#new_data2.to_excel('C:/Users/Ofu/Desktop/研究/BEMS/BEMS&気温3/B11000065.xlsx')#Excelファイルに変換して保存

#2次元平面にマッピング
new_data2.describe()
plt.scatter(new_data2['最高気温'], new_data2['合計'])
plt.ylabel('電気需要')
plt.xlabel('最高気温')
#plt.show()

#k-meansクラスタリング
from sklearn.mixture import GaussianMixture
cust_array = np.array([ new_data2['最高気温'].tolist(), new_data2['合計'].tolist()], np.int32)
cust_array = cust_array.T # 転置行列の作成
pred = KMeans(n_clusters=2).fit_predict(cust_array)
new_data2['cluster_id'] = pred
plt.scatter(new_data2['最高気温'], new_data2['合計'], c=new_data2['cluster_id'])
plt.ylabel('電気需要')
plt.xlabel('最高気温')
plt.show()
new_data2.to_excel('C:/User#s/Ofu/Desktop/研究/BEMS/BEMS&気温3/B11000609.xlsx')

#クラスタごとに表をわける

new_data3 = new_data2.copy()
new_data3 = new_data3.drop(['1時(気温)', '2時(気温)', '3時(気温)', '4時(気温)', '5時(気温)', '6時(気温)', '7時(気温)', '8時(気温)', '9時(気温)', '10時(気温)', '11時(気温)', '12時(気温)', '13時(気温)', '14時(気温)', '15時(気温)', '16時(気温)', '17時(気温)', '18時(気温)', '19時(気温)', '20時(気温)', '21時(気温)', '22時(気温)', '23時(気温)', '24時(気温)', '最高気温'], axis=1)
new_data4 = new_data3.copy()
new_data3 = new_data3.set_index('計測日')
new_data5 = new_data3.copy()

#0クラスタ
new_data3.describe()
new_data3.drop(new_data3.index[new_data3.cluster_id == 0], inplace=True)
new_data3.drop(new_data3.index[new_data3.cluster_id == 1], inplace=True)
new_data3 = new_data3.drop(['合計', 'cluster_id'], axis=1)
new_data3 = new_data3.T
new_data3.plot()
plt.xlabel("計測時間")
plt.ylabel("電気需要")
plt.ylim([0,23])
plt.xticks([6, 12, 18], ['6', '12', '18'])
#plt.show()

new_data4 = new_data4.set_index('計測日')

#1クラスタ
new_data4.describe()
new_data4.drop(new_data4.index[new_data4.cluster_id == 1], inplace=True)
new_data4.drop(new_data4.index[new_data4.cluster_id == 2], inplace=True)
new_data4 = new_data4.drop(['合計', 'cluster_id'], axis=1)
new_data4 = new_data4.T
new_data4.plot()
plt.xlabel("計測時間")
plt.ylabel("電気需要")
plt.ylim([0,23])
plt.xticks([6, 12, 18], ['6', '12', '18'])
#plt.show()


#2クラスタ
new_data5.describe()
new_data5.drop(new_data5.index[new_data5.cluster_id == 2], inplace=True)
new_data5.drop(new_data5.index[new_data5.cluster_id == 0], inplace=True)
new_data5 = new_data5.drop(['合計', 'cluster_id'], axis=1)
new_data5 = new_data5.T
new_data5.plot()
plt.xlabel("計測時間")
plt.ylabel("電気需要")
plt.ylim([0,23])
plt.xticks([6, 12, 18], ['6', '12', '18'])
#plt.show()